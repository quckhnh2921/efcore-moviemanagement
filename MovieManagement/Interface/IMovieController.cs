﻿using MovieManagement.Model;

namespace MovieManagement.UnitOfWork
{
    public interface IMovieController
    {
         string SeeComment(int id);
    }
}
