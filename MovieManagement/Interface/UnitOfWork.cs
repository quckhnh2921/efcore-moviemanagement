﻿namespace MovieManagement.UnitOfWork
{
    public interface UnitOfWork<T>
    {
        string ShowAll();
        T FindById(int id);
        void Add(T entity);
        void Edit(T entity);
        void Delete(T entity);
    }
}
