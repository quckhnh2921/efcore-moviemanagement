﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MovieManagement.Interface
{
    public interface IPaging<T>
    {
         string NextPage(int pageNumber);
         string PreviousPage(int pageNumber);
         void Show(string userInput);
         List<T> Paging(int pageNumber);
    }
}
