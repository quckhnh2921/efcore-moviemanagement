﻿using Microsoft.EntityFrameworkCore;
using MovieManagement.Model;

namespace MovieManagement.Context
{
    public class AppDbContext : DbContext
    {
        public DbSet<Director> Directors { get; set; }
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Comment> Comments { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            string connectionString = "Data Source = KHANHS; Database = MovieManagement; Trusted_Connection = True; TrustServerCertificate = True";
            optionsBuilder.UseSqlServer(connectionString);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Director>()
                .HasMany(author => author.Movies)
                .WithOne(movie => movie.Author)
                .HasForeignKey(movie => movie.AuthorId);

            modelBuilder.Entity<Movie>()
                .HasOne(movie => movie.Author)
                .WithMany(author => author.Movies)
                .HasForeignKey(movie => movie.AuthorId);

            modelBuilder.Entity<Movie>()
                .HasMany(movie => movie.Comments)
                .WithOne(comment => comment.Movie)
                .HasForeignKey(comment => comment.MovieId);

            modelBuilder.Entity<Director>().HasData(
                new Director { Id = 1, Name = "Khanh" },
                new Director { Id = 2, Name = "James Gunn" },
                new Director { Id = 3, Name = "Micheal Bay" },
                new Director { Id = 4, Name = "James Cameron" },
                new Director { Id = 5, Name = "Joss Whedon" }
                );
            modelBuilder.Entity<Movie>().HasData(
                new Movie { Id = 1, AuthorId = 1, Name = "C# Fuck My Life", Country = "Viet Nam", PuplishedDate = DateTime.Now },
                new Movie { Id = 2, AuthorId = 2, Name = "Guardian Of The Galaxy 1", Country = "United State", PuplishedDate = DateTime.Now },
                new Movie { Id = 3, AuthorId = 2, Name = "Guardian Of The Galaxy 2", Country = "United State", PuplishedDate = DateTime.Now },
                new Movie { Id = 4, AuthorId = 2, Name = "Guardian Of The Galaxy 3", Country = "United State", PuplishedDate = DateTime.Now },
                new Movie { Id = 5, AuthorId = 3, Name = "Tranformer Rise Of the Beast", Country = "United State", PuplishedDate = DateTime.Now },
                new Movie { Id = 6, AuthorId = 3, Name = "Tranformer 4", Country = "United State", PuplishedDate = DateTime.Now },
                new Movie { Id = 7, AuthorId = 4, Name = "Avatar: The way of water", Country = "United State", PuplishedDate = DateTime.Now },
                new Movie { Id = 8, AuthorId = 4, Name = "Avatar", Country = "United State", PuplishedDate = DateTime.Now },
                new Movie { Id = 9, AuthorId = 4, Name = "Titanic", Country = "United State", PuplishedDate = DateTime.Now },
                new Movie { Id = 10, AuthorId = 5, Name = "Justice League", Country = "United State", PuplishedDate = DateTime.Now }
                );
            modelBuilder.Entity<Comment>().HasData(
                new Comment { Id = 1, MovieId = 1, Content = "Shit that's a fact" },
                new Comment { Id = 2, MovieId = 1, Content = "Haha that's true" },
                new Comment { Id = 3, MovieId = 2, Content = "Vệ binh dải ngân hề" },
                new Comment { Id = 4, MovieId = 2, Content = "Starlord !!!" },
                new Comment { Id = 5, MovieId = 3, Content = "Starlord !!!" },
                new Comment { Id = 6, MovieId = 4, Content = "Starlord !!!" },
                new Comment { Id = 7, MovieId = 4, Content = "Starlord !!!" },
                new Comment { Id = 8, MovieId = 5, Content = "Optimus's team !!!" },
                new Comment { Id = 9, MovieId = 5, Content = "Megatron's team !!!" },
                new Comment { Id = 10, MovieId = 6, Content = "That's movie is lit" },
                new Comment { Id = 11, MovieId = 6, Content = "Ewwww" },
                new Comment { Id = 12, MovieId = 6, Content = "Miễn phí đặt cược tại FB88" },
                new Comment { Id = 13, MovieId = 7, Content = "Miễn phí đặt cược tại FB88" },
                new Comment { Id = 14, MovieId = 8, Content = "Miễn phí đặt cược tại FB88" },
                new Comment { Id = 15, MovieId = 9, Content = "Miễn phí đặt cược tại FB88" },
                new Comment { Id = 16, MovieId = 10, Content = "Đá gà online tại Vuagacua.com" }
                );
        }
    }
}
