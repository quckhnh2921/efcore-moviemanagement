﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace MovieManagement.Migrations
{
    /// <inheritdoc />
    public partial class FixClassName : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Movies_Authors_AuthorId",
                table: "Movies");

            migrationBuilder.DropTable(
                name: "Authors");

            migrationBuilder.CreateTable(
                name: "Directors",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Directors", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "Directors",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "Khanh" },
                    { 2, "James Gunn" },
                    { 3, "Micheal Bay" },
                    { 4, "James Cameron" },
                    { 5, "Joss Whedon" }
                });

            migrationBuilder.UpdateData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 1,
                column: "PuplishedDate",
                value: new DateTime(2023, 9, 3, 21, 7, 34, 503, DateTimeKind.Local).AddTicks(2194));

            migrationBuilder.UpdateData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 2,
                column: "PuplishedDate",
                value: new DateTime(2023, 9, 3, 21, 7, 34, 503, DateTimeKind.Local).AddTicks(2204));

            migrationBuilder.UpdateData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 3,
                column: "PuplishedDate",
                value: new DateTime(2023, 9, 3, 21, 7, 34, 503, DateTimeKind.Local).AddTicks(2206));

            migrationBuilder.UpdateData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 4,
                column: "PuplishedDate",
                value: new DateTime(2023, 9, 3, 21, 7, 34, 503, DateTimeKind.Local).AddTicks(2207));

            migrationBuilder.UpdateData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 5,
                column: "PuplishedDate",
                value: new DateTime(2023, 9, 3, 21, 7, 34, 503, DateTimeKind.Local).AddTicks(2208));

            migrationBuilder.UpdateData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 6,
                column: "PuplishedDate",
                value: new DateTime(2023, 9, 3, 21, 7, 34, 503, DateTimeKind.Local).AddTicks(2209));

            migrationBuilder.UpdateData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 7,
                column: "PuplishedDate",
                value: new DateTime(2023, 9, 3, 21, 7, 34, 503, DateTimeKind.Local).AddTicks(2211));

            migrationBuilder.UpdateData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 8,
                column: "PuplishedDate",
                value: new DateTime(2023, 9, 3, 21, 7, 34, 503, DateTimeKind.Local).AddTicks(2212));

            migrationBuilder.UpdateData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 9,
                column: "PuplishedDate",
                value: new DateTime(2023, 9, 3, 21, 7, 34, 503, DateTimeKind.Local).AddTicks(2213));

            migrationBuilder.UpdateData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 10,
                column: "PuplishedDate",
                value: new DateTime(2023, 9, 3, 21, 7, 34, 503, DateTimeKind.Local).AddTicks(2214));

            migrationBuilder.AddForeignKey(
                name: "FK_Movies_Directors_AuthorId",
                table: "Movies",
                column: "AuthorId",
                principalTable: "Directors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Movies_Directors_AuthorId",
                table: "Movies");

            migrationBuilder.DropTable(
                name: "Directors");

            migrationBuilder.CreateTable(
                name: "Authors",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Authors", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "Authors",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "Khanh" },
                    { 2, "James Gunn" },
                    { 3, "Micheal Bay" },
                    { 4, "James Cameron" },
                    { 5, "Joss Whedon" }
                });

            migrationBuilder.UpdateData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 1,
                column: "PuplishedDate",
                value: new DateTime(2023, 9, 3, 17, 9, 42, 926, DateTimeKind.Local).AddTicks(4907));

            migrationBuilder.UpdateData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 2,
                column: "PuplishedDate",
                value: new DateTime(2023, 9, 3, 17, 9, 42, 926, DateTimeKind.Local).AddTicks(4918));

            migrationBuilder.UpdateData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 3,
                column: "PuplishedDate",
                value: new DateTime(2023, 9, 3, 17, 9, 42, 926, DateTimeKind.Local).AddTicks(4920));

            migrationBuilder.UpdateData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 4,
                column: "PuplishedDate",
                value: new DateTime(2023, 9, 3, 17, 9, 42, 926, DateTimeKind.Local).AddTicks(4921));

            migrationBuilder.UpdateData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 5,
                column: "PuplishedDate",
                value: new DateTime(2023, 9, 3, 17, 9, 42, 926, DateTimeKind.Local).AddTicks(4922));

            migrationBuilder.UpdateData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 6,
                column: "PuplishedDate",
                value: new DateTime(2023, 9, 3, 17, 9, 42, 926, DateTimeKind.Local).AddTicks(4923));

            migrationBuilder.UpdateData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 7,
                column: "PuplishedDate",
                value: new DateTime(2023, 9, 3, 17, 9, 42, 926, DateTimeKind.Local).AddTicks(4924));

            migrationBuilder.UpdateData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 8,
                column: "PuplishedDate",
                value: new DateTime(2023, 9, 3, 17, 9, 42, 926, DateTimeKind.Local).AddTicks(4925));

            migrationBuilder.UpdateData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 9,
                column: "PuplishedDate",
                value: new DateTime(2023, 9, 3, 17, 9, 42, 926, DateTimeKind.Local).AddTicks(4926));

            migrationBuilder.UpdateData(
                table: "Movies",
                keyColumn: "Id",
                keyValue: 10,
                column: "PuplishedDate",
                value: new DateTime(2023, 9, 3, 17, 9, 42, 926, DateTimeKind.Local).AddTicks(4927));

            migrationBuilder.AddForeignKey(
                name: "FK_Movies_Authors_AuthorId",
                table: "Movies",
                column: "AuthorId",
                principalTable: "Authors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
