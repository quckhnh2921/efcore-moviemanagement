﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace MovieManagement.Migrations
{
    /// <inheritdoc />
    public partial class FixRelationship : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Authors",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Authors", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Movies",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    PuplishedDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    AuthorId = table.Column<int>(type: "int", nullable: false),
                    Country = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Movies", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Movies_Authors_AuthorId",
                        column: x => x.AuthorId,
                        principalTable: "Authors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Comments",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    MovieId = table.Column<int>(type: "int", nullable: false),
                    Content = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Comments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Comments_Movies_MovieId",
                        column: x => x.MovieId,
                        principalTable: "Movies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Authors",
                columns: new[] { "Id", "Name" },
                values: new object[,]
                {
                    { 1, "Khanh" },
                    { 2, "James Gunn" },
                    { 3, "Micheal Bay" },
                    { 4, "James Cameron" },
                    { 5, "Joss Whedon" }
                });

            migrationBuilder.InsertData(
                table: "Movies",
                columns: new[] { "Id", "AuthorId", "Country", "Name", "PuplishedDate" },
                values: new object[,]
                {
                    { 1, 1, "Viet Nam", "C# Fuck My Life", new DateTime(2023, 9, 3, 17, 9, 42, 926, DateTimeKind.Local).AddTicks(4907) },
                    { 2, 2, "United State", "Guardian Of The Galaxy 1", new DateTime(2023, 9, 3, 17, 9, 42, 926, DateTimeKind.Local).AddTicks(4918) },
                    { 3, 2, "United State", "Guardian Of The Galaxy 2", new DateTime(2023, 9, 3, 17, 9, 42, 926, DateTimeKind.Local).AddTicks(4920) },
                    { 4, 2, "United State", "Guardian Of The Galaxy 3", new DateTime(2023, 9, 3, 17, 9, 42, 926, DateTimeKind.Local).AddTicks(4921) },
                    { 5, 3, "United State", "Tranformer Rise Of the Beast", new DateTime(2023, 9, 3, 17, 9, 42, 926, DateTimeKind.Local).AddTicks(4922) },
                    { 6, 3, "United State", "Tranformer 4", new DateTime(2023, 9, 3, 17, 9, 42, 926, DateTimeKind.Local).AddTicks(4923) },
                    { 7, 4, "United State", "Avatar: The way of water", new DateTime(2023, 9, 3, 17, 9, 42, 926, DateTimeKind.Local).AddTicks(4924) },
                    { 8, 4, "United State", "Avatar", new DateTime(2023, 9, 3, 17, 9, 42, 926, DateTimeKind.Local).AddTicks(4925) },
                    { 9, 4, "United State", "Titanic", new DateTime(2023, 9, 3, 17, 9, 42, 926, DateTimeKind.Local).AddTicks(4926) },
                    { 10, 5, "United State", "Justice League", new DateTime(2023, 9, 3, 17, 9, 42, 926, DateTimeKind.Local).AddTicks(4927) }
                });

            migrationBuilder.InsertData(
                table: "Comments",
                columns: new[] { "Id", "Content", "MovieId" },
                values: new object[,]
                {
                    { 1, "Shit that's a fact", 1 },
                    { 2, "Haha that's true", 1 },
                    { 3, "Vệ binh dải ngân hề", 2 },
                    { 4, "Starlord !!!", 2 },
                    { 5, "Starlord !!!", 3 },
                    { 6, "Starlord !!!", 4 },
                    { 7, "Starlord !!!", 4 },
                    { 8, "Optimus's team !!!", 5 },
                    { 9, "Megatron's team !!!", 5 },
                    { 10, "That's movie is lit", 6 },
                    { 11, "Ewwww", 6 },
                    { 12, "Miễn phí đặt cược tại FB88", 6 },
                    { 13, "Miễn phí đặt cược tại FB88", 7 },
                    { 14, "Miễn phí đặt cược tại FB88", 8 },
                    { 15, "Miễn phí đặt cược tại FB88", 9 },
                    { 16, "Đá gà online tại Vuagacua.com", 10 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Comments_MovieId",
                table: "Comments",
                column: "MovieId");

            migrationBuilder.CreateIndex(
                name: "IX_Movies_AuthorId",
                table: "Movies",
                column: "AuthorId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Comments");

            migrationBuilder.DropTable(
                name: "Movies");

            migrationBuilder.DropTable(
                name: "Authors");
        }
    }
}
