﻿using MovieManagement.Controller;
using MovieManagement.MenuDisplay;
using MovieManagement.Model;
using System.Text;

MovieController movieController = new MovieController();
DirectorController directorController = new DirectorController();
Menu menu = new Menu();
int choice = 0;
Console.OutputEncoding = System.Text.Encoding.UTF8;
do
{
	try
	{
        Console.Clear();
        menu.MainMenu();
        choice = int.Parse(Console.ReadLine());
        Console.Clear();
        switch (choice)
        {
            case 1:
                StringBuilder stringBuilder = new StringBuilder();
                int directorChoice = 0;
                do
                {
                    try
                    {
                        Console.Clear ();
                        menu.DirectorMenu();
                        directorChoice = int.Parse(Console.ReadLine());
                        Console.Clear();
                        switch (directorChoice)
                        {
                            case 1:
                                var firstPage = directorController.Paging(1);
                                foreach (var item in firstPage)
                                {
                                    stringBuilder.Append(item.ToString());
                                }
                                Console.WriteLine(stringBuilder.ToString());
                                string userInput = "";
                                do
                                {
                                    menu.PageOptionMenu();
                                    userInput = Console.ReadLine().ToUpper();
                                    Console.Clear();
                                    directorController.Show(userInput);
                                } while (!userInput.Equals("B"));
                                break;
                            case 2:
                                Console.WriteLine("Enter id to find: ");
                                int directorID = int.Parse(Console.ReadLine());
                                var directorFound = directorController.FindById(directorID);
                                Console.WriteLine(directorFound.ToString());
                                Continue();
                                break;
                            case 3:
                                Director newDirector = new Director();
                                Console.WriteLine("Enter name: ");
                                newDirector.Name = Console.ReadLine();
                                directorController.Add(newDirector);
                                Continue();
                                break;
                            case 4:
                                Console.WriteLine("Enter id to edit: ");
                                var directorEdit = directorController.FindById(int.Parse(Console.ReadLine()));
                                Console.WriteLine("Enter new name ");
                                directorEdit.Name = Console.ReadLine();
                                directorController.Edit(directorEdit);
                                Continue();
                                break;
                            case 5:
                                Console.WriteLine("Enter id to remove: ");
                                var directorRemove = directorController.FindById(int.Parse(Console.ReadLine()));
                                Console.WriteLine(directorRemove.ToString());
                                Console.WriteLine("Do you want to remove(Y/N) ?");
                                string removeChoice = Console.ReadLine().ToUpper();
                                if (!removeChoice.Equals("Y") && !removeChoice.Equals("N"))
                                {
                                    throw new Exception("Please enter Y or N ");
                                }
                                if (removeChoice.Equals("Y"))
                                {
                                    directorController.Delete(directorRemove);
                                }
                                Continue();
                                break;
                            case 6:
                                break;
                            default:
                                throw new Exception("Please enter from 1 to 6");
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine($"Error: {ex.Message}");
                        Continue();
                    }
                } while (directorChoice != 6);
                break;
            case 2:
                int movieChoice = 0;
                StringBuilder sb = new StringBuilder();
                do
                {
                    try
                    {
                        Console.Clear();
                        menu.MovieMenu();
                        movieChoice = int.Parse(Console.ReadLine());
                        Console.Clear();
                        switch (movieChoice)
                        {
                            case 1:
                                var firstPage = movieController.Paging(1);
                                foreach (var item in firstPage)
                                {
                                    sb.Append(item.ToString());
                                }
                                Console.WriteLine(sb.ToString());
                                string userInput = "";
                                do
                                {
                                    menu.PageOptionMenu();
                                    userInput = Console.ReadLine().ToUpper();
                                    Console.Clear();
                                    movieController.Show(userInput);
                                } while (!userInput.Equals("B"));
                                break;
                            case 2:
                                Console.WriteLine("Enter id to find: ");
                                int movieFound = int.Parse(Console.ReadLine());
                                var details = movieController.FindById(movieFound);
                                Console.WriteLine(details);
                                Console.WriteLine("Comments: ");
                                var comment = movieController.SeeComment(movieFound);
                                Console.WriteLine(comment);
                                Continue();
                                break;
                            case 3:
                                Movie movie = new Movie();
                                Console.WriteLine("Enter name of movie: ");
                                movie.Name = Console.ReadLine();
                                Console.WriteLine("Enter publised date: ");
                                movie.PuplishedDate = DateTime.Parse(Console.ReadLine());
                                Console.WriteLine("Enter country: ");
                                movie.Country = Console.ReadLine();
                                Console.WriteLine("Enter Author id: ");
                                movie.AuthorId = int.Parse(Console.ReadLine());
                                directorController.FindById(movie.AuthorId);
                                movieController.Add(movie);
                                Continue();
                                break;
                            case 4:
                                Console.WriteLine("Enter id to edit: ");
                                var movieEdit = movieController.FindById(int.Parse(Console.ReadLine()));
                                Console.WriteLine("Enter name of movie: ");
                                movieEdit.Name = Console.ReadLine();
                                Console.WriteLine("Enter publised date: ");
                                movieEdit.PuplishedDate = DateTime.Parse(Console.ReadLine());
                                Console.WriteLine("Enter contry: ");
                                movieEdit.Country = Console.ReadLine();
                                Console.WriteLine("Enter Author id: ");
                                movieEdit.AuthorId = int.Parse(Console.ReadLine());
                                directorController.FindById(movieEdit.AuthorId);
                                movieController.Edit(movieEdit);
                                Continue();
                                break;
                            case 5:
                                Console.WriteLine("Enter id to remove: ");
                                var movieRemove = movieController.FindById(int.Parse(Console.ReadLine()));
                                Console.WriteLine("Do you want to remove(Y/N) ?");
                                string removeChoice = Console.ReadLine().ToUpper();
                                if (!removeChoice.Equals("Y") && !removeChoice.Equals("N"))
                                {
                                    throw new Exception("Please enter Y or N ");
                                }
                                if (removeChoice.Equals("Y"))
                                {
                                    movieController.Delete(movieRemove);
                                    Console.WriteLine("Remove sucessful !");
                                }
                                Continue();
                                break;
                            case 6:
                                break;
                            default:
                                throw new Exception("Please enter from 1 to 6");
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine($"Error: {ex.Message}");
                        Continue();
                    }
                } while (movieChoice != 6);
                break;
            case 3:
                Environment.Exit(0);
                break;
            default:
                throw new Exception("Please enter from 1 to 3");
        }
    }
	catch (Exception ex)
	{
        Console.WriteLine($"Error: {ex.Message}");
        Continue();
    }
} while (choice != 3);



void Continue()
{
    Console.WriteLine("Press any key to continue !");
    Console.ReadKey();
}