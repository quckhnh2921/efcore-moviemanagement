﻿using System.Text;

namespace MovieManagement.Model
{
    public class Director
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<Movie> Movies { get; set; }
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append($"{Id} - {Name} - Has {Movies.Count} movies\n");
            return sb.ToString();
        }
    }
}
