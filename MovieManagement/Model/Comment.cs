﻿using System.Text;

namespace MovieManagement.Model
{
    public class Comment
    {
        public int Id { get; set; }
        public int MovieId { get; set; }
        public Movie Movie { get; set; }
        public string Content { get; set; }
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append($"\t{Content}\n");
            return sb.ToString();
        }
    }
}
