﻿using System.Text;

namespace MovieManagement.Model
{
    public class Movie
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime PuplishedDate { get; set; }
        public Director Author { get; set; }
        public int AuthorId { get; set; }
        public string Country { get; set; }
        public List<Comment> Comments { get; set; }
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append($"{Id} - {Name} - {PuplishedDate.ToString("dd/MM/yyyy")} - {Country} - {Author.Name}\n");
            return sb.ToString();
        }
    }
}
