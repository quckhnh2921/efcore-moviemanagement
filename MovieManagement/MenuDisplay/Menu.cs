﻿namespace MovieManagement.MenuDisplay
{
    public class Menu
    {
        public void MainMenu()
        {
            Console.WriteLine("================MAIN=MENU================");
            Console.WriteLine("| 1. Directors                          |");
            Console.WriteLine("| 2. Movies                             |");
            Console.WriteLine("| 3. Exit                               |");
            Console.WriteLine("=========================================");
            Console.Write("User choice: ");
        }
        public void DirectorMenu()
        {
            Console.WriteLine("==================DIRECTOR=MENU==================");
            Console.WriteLine("| 1. Show all directors                         |");
            Console.WriteLine("| 2. Find director                              |");
            Console.WriteLine("| 3. Add new director                           |");
            Console.WriteLine("| 4. Edit director                              |");
            Console.WriteLine("| 5. Remove director                            |");
            Console.WriteLine("| 6. Exit                                       |");
            Console.WriteLine("=================================================");
            Console.Write("User choice: ");
        }
        public void MovieMenu()
        {
            Console.WriteLine("=================MOVIE=MENU==================");
            Console.WriteLine("| 1. Show all movies                        |");
            Console.WriteLine("| 2. Find movie                             |");
            Console.WriteLine("| 3. Add new movie                          |");
            Console.WriteLine("| 4. Edit movie                             |");
            Console.WriteLine("| 5. Remove movie                           |");
            Console.WriteLine("| 6. Exit                                   |");
            Console.WriteLine("=============================================");
            Console.Write("User choice: ");
        }

        public void PageOptionMenu()
        {
            Console.WriteLine("N to next");
            Console.WriteLine("P to previous");
            Console.WriteLine("B to back to main menu");
        }
    }
}
