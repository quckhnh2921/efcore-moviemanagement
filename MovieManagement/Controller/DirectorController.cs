﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Primitives;
using MovieManagement.Context;
using MovieManagement.Interface;
using MovieManagement.Model;
using MovieManagement.UnitOfWork;
using System.Text;

namespace MovieManagement.Controller
{
    public class DirectorController : UnitOfWork<Director>, IPaging<Director>
    {
        private readonly AppDbContext context;
        private int pageNumber = 1;
        private int pageSize = 3;
        public DirectorController()
        {
            context = new AppDbContext();
        }
        public void Add(Director entity)
        {
            var add = context.Directors.Add(entity);
            if (add is not null)
            {
                Console.WriteLine("Add successful");
            }
            context.SaveChanges();
        }

        public void Delete(Director entity)
        {
            var delete = context.Directors.Remove(entity);
            if (delete is not null)
            {
                Console.WriteLine("Remove successful");
            }
            context.SaveChanges();
        }

        public void Edit(Director entity)
        {
            var edit = context.Directors.Update(entity);
            if (edit is not null)
            {
                Console.WriteLine("Edit sucessful");
            }
            context.SaveChanges();
        }

        public Director FindById(int id)
        {
            var director = context.Directors.Include(movie => movie.Movies).SingleOrDefault(director => director.Id == id);
            if (director is null)
            {
                throw new Exception($"Can not found director by id {id}");
            }
            return director;
        }
        public string NextPage(int pageNumber)
        {
            StringBuilder sb = new StringBuilder();
            var nextPage = Paging(pageNumber);
            foreach (var director in nextPage)
            {
                sb.Append(director.ToString());
            }
            return sb.ToString();
        }

        public List<Director> Paging(int pageNumber)
        {
            var directors = context.Directors.Include(movie => movie.Movies).OrderBy(director => director.Id).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();
            return directors;
        }

        public string PreviousPage(int pageNumber)
        {
            StringBuilder sb = new StringBuilder();
            var prePage = Paging(pageNumber);
            foreach (var director in prePage)
            {
                sb.Append(director.ToString());
            }
            return sb.ToString() ;
        }

        public void Show(string userInput)
        {
            StringBuilder sb = new StringBuilder();
            var totalDirector = context.Directors.ToList().Count();
            double totalPage = Math.Ceiling((totalDirector * 1.0) / pageSize);
            switch (userInput)
            {
                case "N":
                    pageNumber++;
                    if (pageNumber > totalPage)
                    {
                        pageNumber = 1;
                        var firstPage = Paging(pageNumber);
                        foreach (var director in firstPage)
                        {
                            sb.Append(director.ToString());
                        }
                        Console.WriteLine(sb.ToString());
                    }
                    else
                    {
                        sb.Append(NextPage(pageNumber));
                        Console.WriteLine(sb.ToString());
                    }
                    break;
                case "P":
                    pageNumber--;
                    if (pageNumber < 1)
                    {
                        pageNumber = (int)totalPage;
                        var lastPage = Paging(pageNumber);
                        foreach (var director in lastPage)
                        {
                            sb.Append(director.ToString());
                        }
                        Console.WriteLine(sb.ToString());
                    }
                    else
                    {
                        sb.Append(PreviousPage(pageNumber));
                        Console.WriteLine(sb.ToString());
                    }
                    break;
                case "B":
                    pageNumber = 1;
                    break;
                default:
                    throw new Exception("Please enter N/P/B");
            }
        }
        public string ShowAll()
        {
            StringBuilder sb = new StringBuilder();
            var directors = context.Directors.Include(movie => movie.Movies).ToList();
            foreach (var director in directors)
            {
                sb.Append($"{director.ToString()}");
            }
            return sb.ToString();
        }
    }
}
