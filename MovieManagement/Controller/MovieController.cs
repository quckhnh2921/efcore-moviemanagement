﻿using Microsoft.EntityFrameworkCore;
using MovieManagement.UnitOfWork;
using MovieManagement.Model;
using System.Text;
using MovieManagement.Context;
using MovieManagement.Interface;

namespace MovieManagement.Controller
{
    public class MovieController : IMovieController, UnitOfWork<Movie>, IPaging<Movie>
    {
        private readonly AppDbContext context;
        private int pageNumber = 1;
        private int pageSize = 3;
        public MovieController()
        {
            context = new AppDbContext();
        }
        public void Add(Movie movie)
        {
            var add = context.Movies.Add(movie);
            if (add is not null)
            {
                Console.WriteLine("Add sucessful");
            }
            context.SaveChanges();
        }

        public void Delete(Movie movie)
        {
            var delete = context.Movies.Remove(movie);
            if (delete is not null)
            {
                Console.WriteLine("Delete sucessful");
            }
            context.SaveChanges();
        }

        public void Edit(Movie movie)
        {
            var edit = context.Movies.Update(movie);
            if (edit is not null)
            {
                Console.WriteLine("Edit sucessful");
            }
            context.SaveChanges();
        }

        public string ShowAll()
        {
            StringBuilder sb = new StringBuilder();
            var allMovies = context.Movies.Include(movie => movie.Author).ToList().GroupBy(movie => movie.Author.Name);
            foreach (var director in allMovies)
            {
                sb.Append($"\nDirector: {director.Key}");
                foreach (var movie in director)
                {
                    sb.Append($"\n\t{movie.ToString()}");
                }
            }
            return sb.ToString();
        }
        public Movie FindById(int id)
        {
            var movie = context.Movies.Include(movie => movie.Author).SingleOrDefault(movie => movie.Id == id);
            if (movie is null)
            {
                throw new Exception($"Can not found movie by id {id}");
            }
            return movie;
        }

        public string SeeComment(int id)
        {
            StringBuilder sb = new StringBuilder();
            var movie = FindById(id);
            //explict load comment
            context.Entry(movie).Collection(movie => movie.Comments).Load();
            foreach (var comment in movie.Comments)
            {
                sb.Append($"{comment.ToString()}");
            }
            return sb.ToString();
        }

        public string NextPage(int pageNumber)
        {
            StringBuilder sb = new StringBuilder();
            var nextPage = Paging(pageNumber);
            foreach (var movie in nextPage)
            {
                sb.Append(movie.ToString());
            }
            return sb.ToString();
        }

        public string PreviousPage(int pageNumber)
        {
            StringBuilder sb = new StringBuilder();
            var prePage = Paging(pageNumber);
            foreach (var movie in prePage)
            {
                sb.Append(movie.ToString());
            }
            return sb.ToString();
        }

        public void Show(string userInput)
        {
            var totalMovie = context.Movies.ToList().Count();
            double totalPage = Math.Ceiling((totalMovie * 1.0) / pageSize);
            StringBuilder sb = new StringBuilder();
            switch (userInput)
            {
                case "N":
                    pageNumber++;
                    if (pageNumber >= totalPage)
                    {
                        pageNumber = 1;
                        var firstPage = Paging(pageNumber);
                        foreach (var movie in firstPage)
                        {
                            sb.Append(movie.ToString());
                        }
                        Console.WriteLine(sb.ToString());
                    }
                    else
                    {
                        sb.Append(NextPage(pageNumber));
                        Console.WriteLine(sb.ToString());
                    }
                    break;
                case "P":
                    pageNumber--;
                    if (pageNumber < 1)
                    {
                        pageNumber = (int)totalPage;
                        var lastPage = Paging(pageNumber);
                        foreach (var movie in lastPage)
                        {
                            sb.Append(movie.ToString());
                        }
                        Console.WriteLine(sb.ToString());
                    }
                    else
                    {
                        sb.Append(PreviousPage(pageNumber));
                        Console.WriteLine(sb.ToString());
                    }
                    break;
                case "B":
                    pageNumber = 1;
                    break;
                default:
                    throw new Exception("Please enter N/P/B");
            }
        }

        public List<Movie> Paging(int pageNumber)
        {
            var movies = context.Movies.Include(director => director.Author).OrderBy(movie => movie.Id).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();
            return movies;
        }
    }
}
